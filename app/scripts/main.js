(function () {

// Dummy data that would come from your database via ajax, or in localStorage
var database = {

    'username': 'Luke',
    'mixes':[
      {
        id: 0,
        title: 'DnB Mix',
        src: 'http://www.stephaniequinn.com/Music/Mouret%20-%20Rondeau.mp3', // Or local path
        thumbnail: 'images/cover.jpg'
      },
      {
        id: 1,
        title: 'House Mix',
        src: 'http://www.stephaniequinn.com/Music/Mouret%20-%20Rondeau.mp3', // Or local path
        thumbnail: 'images/cover.jpg'

      },
      {
        id: 2,
        title: 'Hip Hop Mix',
        src: 'http://www.stephaniequinn.com/Music/Mouret%20-%20Rondeau.mp3', // Or local path
        thumbnail: 'images/cover.jpg'
      },
      {
        id: 3,
        title: 'Trance Mix',
        src: 'http://www.stephaniequinn.com/Music/Mouret%20-%20Rondeau.mp3', // Or local path
        thumbnail: 'images/cover.jpg'
      }
    ]
  };

  /**
   *
   */
  function render() {

    var html = '';
    var target = document.getElementById('app');

    database.mixes.forEach(function(el) {

      html +=
        '<div class="col-sm-6">' +
        '<img class="img img-responsive" src="' + el.thumbnail + '" alt="">' +
        '<h4>' + el.title + '</h4>' +
        '<audio controls>' +
        '<source src="' + el.src + '" type="audio/mpeg">'+
        '</audio>' +
        '</div>';
    });

    target.innerHTML = '';
    target.innerHTML = html;

  }

  /**
   * Dummy submit
   */
  function addNewMixListener() {

    var submitButton = document.getElementsByClassName('btn-primary')[0];
    var mixTitle = document.getElementById('title');
    var mixSrc = document.getElementById('uploader');
    var newMix = {
      id: 0,
      title: '',
      src: '',
      thumbnail: 'images/cover.jpg'
    };

    submitButton.addEventListener('click', function() {

      newMix.id = database.mixes.length;
      newMix.title = mixTitle.value;
      newMix.src = mixSrc.value;

      database.mixes.push(newMix);

      render();

    },false);

  }

  // init
  render();
  addNewMixListener();

})();








